package com.example.yeddl.yeddluriharishmovies.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.yeddl.yeddluriharishmovies.R;
import com.example.yeddl.yeddluriharishmovies.models.Movies;
import com.example.yeddl.yeddluriharishmovies.network.MoviesNetworkManager;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yeddl on 1/29/2017.
 */

public class ListFragment extends Fragment {
    RecyclerView listRecyclerView;
    RecyclerView.LayoutManager layoutManager;
    ListFragmentAdapter listFragmentAdapter;
    List<String> moviesList;
    String searchTitle;
    private boolean dualPane;
    MoviesNetworkManager moviesNetworkManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        searchTitle=getArguments().getString("title");
        Log.e("SearchTitle","Title: "+searchTitle);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.list_fragment,container,false);

        moviesNetworkManager=new MoviesNetworkManager();
        fetchMoviesList();
        if (view.findViewById(R.id.detailFrameLayout)!=null){
            dualPane=true;
        }
        listRecyclerView= (RecyclerView) view.findViewById(R.id.moviesListRecyclerView);
        layoutManager=new LinearLayoutManager(this.getActivity());
        listRecyclerView.setLayoutManager(layoutManager);
        return view;
    }

    private void fetchMoviesList() {
        Call<Movies> moviesListCall=moviesNetworkManager.getmOmdbServices().getMoviesList(searchTitle);
        moviesListCall.enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                if (response.isSuccessful()){
                    Log.e("onResponse","Success: "+response.body()+" Count: ");
                    if (response.body().getSearch()==null){
                        Toast.makeText(getActivity(),"Dude! Don't be lazy, type a word!!",Toast.LENGTH_SHORT).show();
                    }else {
                        listFragmentAdapter = new ListFragmentAdapter(response.body().getSearch(), getActivity(),dualPane);
                        listRecyclerView.setAdapter(listFragmentAdapter);

                    }
                }else {
                    Log.e("onResponse","Custom error code: "+response.code());
                }
            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    Log.e("onFailure", "No Network" + t.getMessage());
                } else if (t instanceof SocketTimeoutException) {
                    Log.e("onResponse", "TimeoutException" + t.getMessage());
                } else {
                    Log.e("onResponse", "Error" + t.getMessage());
                }
            }
        });

    }
}
