package com.example.yeddl.yeddluriharishmovies.network;

import com.example.yeddl.yeddluriharishmovies.network.services.OmdbServices;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by yeddl on 1/31/2017.
 */

public class MoviesNetworkManager {
    public static final String BaseURL="http://www.omdbapi.com";

    private Retrofit mRetrofit;

    private OmdbServices mOmdbServices;
    public MoviesNetworkManager() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BaseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mOmdbServices = mRetrofit.create(OmdbServices.class);
    }
    public OmdbServices getmOmdbServices(){
        return mOmdbServices;
    }
}
