package com.example.yeddl.yeddluriharishmovies.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yeddl.yeddluriharishmovies.R;
import com.example.yeddl.yeddluriharishmovies.models.MovieDetailedInfo;
import com.example.yeddl.yeddluriharishmovies.network.MoviesNetworkManager;
import com.squareup.picasso.Picasso;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by yeddl on 1/29/2017.
 */

public class DetailedFragment extends Fragment {
    TextView imdbIDTV,detailTitleTV,detailYearTV,movieActorsTV,movieDirectorTV,movieGenreTV,movieRuntimeTV,moviePlotTV;
    ImageView detialImage;
    String imdbID;
    MoviesNetworkManager moviesNetworkManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imdbID=getArguments().getString("date");
        setHasOptionsMenu(false);
        moviesNetworkManager=new MoviesNetworkManager();
        fetchMovieDetials();
    }

    private void fetchMovieDetials() {
        Call<MovieDetailedInfo> movieDetailedInfoCall=moviesNetworkManager.getmOmdbServices().getMovieInfo(imdbID);
        movieDetailedInfoCall.enqueue(new Callback<MovieDetailedInfo>() {
            @Override
            public void onResponse(Call<MovieDetailedInfo> call, Response<MovieDetailedInfo> response) {
                if (response.isSuccessful()){
                    Log.e("onResponse","Success: "+response.body());
//                    imdbIDTV,detailTitleTV,detailYearTV,movieActorsTV,movieDirectorTV,movieGenreTV,movieRuntimeTV;
                    detailTitleTV.setText("Title: "+response.body().getTitle());
                    detailYearTV.setText("Year: "+response.body().getYear());
                    movieActorsTV.setText("Actor(s): "+response.body().getActors());
                    movieDirectorTV.setText("Director: "+response.body().getDirector());
                    movieGenreTV.setText("Genre: "+response.body().getGenre());
                    movieRuntimeTV.setText("Runtime: "+response.body().getRuntime());
                    moviePlotTV.setText("Plot: "+response.body().getPlot());
                    Picasso.with(getActivity()).load(response.body().getPoster()).fit().error(R.drawable.dp).into(detialImage);
                }else {
                    Log.e("onResponse","Custom error code: "+response.code());
                }
            }

            @Override
            public void onFailure(Call<MovieDetailedInfo> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    Log.e("onFailure", "No Network" + t.getMessage());
                } else if (t instanceof SocketTimeoutException) {
                    Log.e("onResponse", "TimeoutException" + t.getMessage());
                } else {
                    Log.e("onResponse", "Error" + t.getMessage());
                }
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.items, menu);
//        menu.findItem(R.id.searchItem).setVisible(false);
        menu.clear();
//        menu.getItem(R.id.searchItem).setEnabled(false); // here pass the index of save menu item
        return ;

    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.detailed_fragment,container,false);
//        imdbIDTV= (TextView) view.findViewById(R.id.dateTV);
        detailTitleTV= (TextView) view.findViewById(R.id.detailTitleTV);
        detailYearTV= (TextView) view.findViewById(R.id.detailYearTV);
        movieActorsTV= (TextView) view.findViewById(R.id.movieActorsTV);
        movieDirectorTV= (TextView) view.findViewById(R.id.movieDirectorTV);
        movieGenreTV= (TextView) view.findViewById(R.id.movieGenreTV);
        movieRuntimeTV= (TextView) view.findViewById(R.id.movieRuntimeTV);
        moviePlotTV= (TextView) view.findViewById(R.id.moviePlotTV);
        detialImage= (ImageView) view.findViewById(R.id.detailImageView);

        movieActorsTV.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        movieActorsTV.setText(imdbID);

        return view;
    }
//    detailTitleTV,detailYearTV,movieActorsTV,movieDirectorTV,movieGenreTV,movieRuntimeTV;
}
