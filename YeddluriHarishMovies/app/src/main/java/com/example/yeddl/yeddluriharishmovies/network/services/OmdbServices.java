package com.example.yeddl.yeddluriharishmovies.network.services;

import com.example.yeddl.yeddluriharishmovies.models.MovieDetailedInfo;
import com.example.yeddl.yeddluriharishmovies.models.Movies;
import com.example.yeddl.yeddluriharishmovies.models.MoviesList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by yeddl on 1/31/2017.
 */

public interface OmdbServices {
    @GET("/")
    Call<Movies> getMoviesList(@Query("s") String movieTitle);

    @GET("/")
    Call<MovieDetailedInfo> getMovieInfo(@Query("i") String imdbID);
}
