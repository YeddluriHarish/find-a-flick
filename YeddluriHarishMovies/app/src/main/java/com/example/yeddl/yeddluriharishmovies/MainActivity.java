package com.example.yeddl.yeddluriharishmovies;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.yeddl.yeddluriharishmovies.fragments.ListFragment;

public class MainActivity extends AppCompatActivity {

    TextView infoTV;
    EditText searchMovieET;
    Button searchBTN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        infoTV= (TextView) findViewById(R.id.infoTV);
        searchMovieET= (EditText) findViewById(R.id.searchMovieET);
        searchBTN= (Button) findViewById(R.id.searchBTN);
        infoTV.setText("Search for any movie by clicking on the search icon at the top!!");
        searchMovieET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    searchBTN.performClick();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });
        searchBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListFragment listFragment=new ListFragment();
                Bundle bundle=new Bundle();
                bundle.putString("title",searchMovieET.getText().toString());
                listFragment.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.activity_main,listFragment).addToBackStack(null).commit();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.items, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.searchItem:
                Log.e("Search","Search Clicked!");
                searchMovieET.setVisibility(View.VISIBLE);
                infoTV.setVisibility(View.GONE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void sendDataToDetail(){

    }
}
