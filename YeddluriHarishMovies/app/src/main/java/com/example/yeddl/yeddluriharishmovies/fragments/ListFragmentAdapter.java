package com.example.yeddl.yeddluriharishmovies.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yeddl.yeddluriharishmovies.R;
import com.example.yeddl.yeddluriharishmovies.models.MoviesList;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by yeddl on 1/29/2017.
 */

public class ListFragmentAdapter  extends RecyclerView.Adapter<ListFragmentAdapter.ViewHolder>{
    int index=0;
    List<MoviesList> moviesInfoList;
    Context context;
    Boolean dualPane=false;

    public ListFragmentAdapter(List<MoviesList> moviesInfoList, Context context,boolean dualPane) {
        this.moviesInfoList=moviesInfoList;
        this.context=context;
        this.dualPane=dualPane;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_list_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        viewHolder.holderIndex=index;
        index++;
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.dateForNasaTV.setText(moviesInfoList.get(position).getTitle());
        holder.movieYearTV.setText(moviesInfoList.get(position).getYear());
        final String imdbID=moviesInfoList.get(position).getImdbID();
        Picasso.with(context).load(moviesInfoList.get(position).getPoster()).error(R.drawable.dp).fit().into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity= (AppCompatActivity) v.getContext();
                DetailedFragment detailedFragment=new DetailedFragment();

                Bundle bundle = new Bundle();
                bundle.putString("date", imdbID);
                detailedFragment.setArguments(bundle);
                if (dualPane){
                    activity.getFragmentManager().beginTransaction().replace(R.id.detailFrameLayout, detailedFragment).addToBackStack(null).commit();
                }else {
                    activity.getFragmentManager().beginTransaction().replace(R.id.activity_main, detailedFragment).addToBackStack(null).commit();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesInfoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView dateForNasaTV,movieYearTV;
        public ImageView imageView;
        public int holderIndex;

        public ViewHolder(View itemView) {
            super(itemView);
            movieYearTV= (TextView) itemView.findViewById(R.id.movieYearTV);
            imageView= (ImageView) itemView.findViewById(R.id.imageView);
            dateForNasaTV= (TextView) itemView.findViewById(R.id.listInfoTV);
        }
    }
}
